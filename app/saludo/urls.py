# greetings/urls.py
from django.conf.urls import url

from .views import index  #Importamos la funcion index de views


urlpatterns = [
    url(r'^$', index),
]